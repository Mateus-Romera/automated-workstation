# Get Makefile full path
mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
# Get Makefile dir name only
root_dir := $(notdir $(patsubst %/,%,$(dir $(mkfile_path))))

# Force to use buildkit for all images and for docker-compose to invoke
# Docker via CLI (otherwise buildkit isn't used for those images)
export DOCKER_BUILDKIT=1
export COMPOSE_DOCKER_CLI_BUILD=1

# Default vagrantFile to use
vagrantFile_path := 'vagrant/ubuntu/focal64/virtualbox/'


# HELP
# This will output the help for each task
.PHONY: help -

# Automated helper
help: ## Show this help information.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

-: ## 

# Easy commands
start: vm exec ## Start vagrant VM and ansible container in interactive mode.

terminate: down vm-halt ## Stop and remove the ansible container. Also halt vagrant's VM.

erase: down vm-halt clean vm-destroy ## Stop and erase everything from this project (destroy VM as well).

-: ## 

# LOCAL / DEV - docker-compose commands

up: ## Start development enviroment.
	@docker-compose up -d

attach: ## Attach to already running ansible container.
	@docker-compose exec ansible bash

exec: up attach ## Start ansible container in interactive mode (attached to container).

logs: ## Display logs from all containers with timestamp.
	@docker-compose logs -t

ps: ## List all containers from project.
	@docker-compose ps -a

build: ## Build docker images from project for development.
	@docker-compose build

build-nc: ## Rebuild docker images (ignore existing cache).
	@docker-compose build --no-cache

recreate: ## Force recreate all containers.
	@docker-compose up --force-recreate -d

reload: build ## Build and force recreate all containers (using cache).
	@docker-compose up --force-recreate -d

reload-nc: build-nc ## Build and force recreate all containers (ignore existing cache).
	@docker-compose up --force-recreate -d

clean: ## Clean all dangling docker images.
	@docker image prune -f

down: ## Stop and remove all containers.
	@docker-compose down --remove-orphans

DOCKERFILE ?= Dockerfile
lint-dockerfile: ## Lint any supplied Dockerfile.
	@docker run \
		--rm \
		--interactive \
		--volume "${PWD}/.hadolint.yml:/.hadolint.yaml:ro" \
			hadolint/hadolint < ${DOCKERFILE}

# lint-ansible: build ## Lint any supplied ansible file.
# 	@docker run \
# 		--rm \
# 		--interactive \
# 		--volume "${PWD}/ansible-lint:/home/ansible/src/ansible-lint:ro" \
# 			ansible-community/toolset < ${DOCKERFILE}

-: ## 

# Vagrant
vm: ## Start vagrant VM.
	@cd $(vagrantFile_path); \
	vagrant up

vm-ssh: ## SSH in vagrant VM.
	@cd $(vagrantFile_path); \
	vagrant ssh

vm-reload: ## Reload vagrant VM.
	@cd $(vagrantFile_path); \
	vagrant reload

vm-provision: ## Provision vagrant VM.
	@cd $(vagrantFile_path); \
	vagrant provision

vm-halt: ## Stop vagrant VM.
	@cd $(vagrantFile_path); \
	vagrant halt

vm-destroy: ## Destroy vagrant VM.
	@cd $(vagrantFile_path); \
	vagrant destroy