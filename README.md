## Ansible playbook to configure and manage my personal Workstation

This repository contains an ansible playbook with multiples tasks to configure and manage my personal Workstation **(Pop!_OS 20.04)**.

It also contains an ansible host inside a docker container and one virtual machine using vagrant (vagrantFile for both libvirt and virtualbox providers) for quick testing purposes.

## Ansible playbook:

### Bootstrap playbook (should be run only once):

- install latest python3;
- ensure openssh-server is installed;
- ensure 'wheel' group is created;
- allow 'wheel' group to have passwordless sudo;
- create an *ansible_user* (to keep log files);
- set authorized key for *ansible_user* copying it from current user (ansible container);

### Main playbook:

##### Tasks:

- create *locale* locale; (fix error)
- set timezone to *timezone*; (fix error)
- upgrade system with latest updates;
- install some system required packages;
- create *main_user*;
- create SSH key for *main_user*;
- disable SSH password authentication;
- fix media keys delay by disabling 'Scroll Lock' from keyboard;
- fix primary monitor with gdm3 startup when using multiple monitors;
- change default keyboard layout to 'pt-br';
- configure the gnome favorite-apps bar for the *main_user*;
- set default audio output source;
- install some .deb packages:
    - google-chrome-stable (latest);
    - teamviewer (latest);
    - gnome-tweaks (latest);
    - virtualbox (latest);
    - qemu-kmv (latest);
    - libvirt-daemon-system (latest);
    - libvirt-clients (latest);
    - bridge-utils (latest);
    - virtinst (latest);
    - virt-manager (latest);
- install flatpak cli;
- install some flatpak packages:
    - code (latest);
    - spotify (latest);
    - gimp (latest);
    - teams_for_linux (latest);
    - transmissionbt (latest);
    - discord (latest);
- install snapd:
- install some snap packages:
    - walc (latest);
    - bpytop (latest);
    - microk8s (latest);
        maybe change for kubectl (latest); ?

##### Roles:

- configure keyboard (Thanks to https://github.com/gantsign/ansible-role-keyboard);
- install gnome extensions (Thanks to https://github.com/jaredhocutt/ansible-gnome-extensions):
    - Auto Move Windows
    - Extensions
    - Freon
    - GameMode
    - Multi Monitors Add-On
    - Workspace Scroll

## Quickstart

You can test every script against a ready Vagrant VM included in this repository.

First, install [Virtual Box](https://www.virtualbox.org/) and [Vagrant](https://www.vagrantup.com/) on your machine:

```bash
sudo apt update \
    && sudo apt install virtualbox \
    && curl -O https://releases.hashicorp.com/vagrant/2.2.14/vagrant_2.2.14_x86_64.deb \
    && sudo apt install ./vagrant_2.2.14_x86_64.deb
```

You also need to install `vagrant-disksize` plugin. This way we can give a little more space to our VM:

```
vagrant plugin install vagrant-disksize
```

Next, replace `ip: "xxx.xxx.xxx.xxx"` with any IP address inside your local network inside the `inventories/staging.yml` inventory file.

Use the `ip addr` command on terminal to list your current IP address and your network interfaces:

```yml
staging:
    hosts:
        vagrant:
            ansible_host: xxx.xxx.xxx.xxx # example: 10.0.0.150
                    ...
```

Now, include the **same** IP address used before and your host network adapter inside the `vagrant/VagrantFile` file.

```ruby
config.vm.network "public_network", ip: "xxx.xxx.xxx.xxx", bridge: [ # same IP address
    "your-adapter-here" # example: "eno1" (onboard interface ethernet port 1) or "wlp2s0" (wifi pci-slot=2 card-index=0)
]
```

Take a look at the [Consistent Network Device Naming](https://en.wikipedia.org/wiki/Consistent_Network_Device_Naming) convention used in Linux if you want to know more.

Use the following command to start the ansible container and the vagrant VM:

```
make start
```

This VM already installs the POP!_OS desktop (and some virtualbox guest addtions for the virtualbox provider only), so the first provision can take quite a while.

After the VM initialization, you will be inside the ansible container automatically.

At last, test the connection with your VM using SSH password authentication:

```
ansible -m ping all -vv -i inventories/staging.yml --extra-vars "variable_ssh_user=vagrant" --ask-pass
```

**Congratulations! If everything worked correctly, you can run this playbook against the VM.**

First, execute the `bootstrap.yml` playbook against your VM to install the basic Ansible requisites (Python 3.x) and to create the Ansible user responsible for managing your host:

```
ansible-playbook bootstrap.yml -vv -i inventories/staging.yml --extra-vars "variable_ssh_user=vagrant" --ask-pass
```

Finally, run the `playbook.yml` playbook to execute every task and roles described in this repository:

```
ansible-playbook playbook.yml -vv -i inventories/staging.yml
```

You can also execute single roles/tasks using tags:

```
ansible-playbook playbook.yml --tags common -vv -i inventories/staging.yml
```

## Setup with your own configs

To use this playbook against your host, follow the instructions below.

First you will have to add your managed node to the `inventories/production.yml` file.

```yml
production:
    hosts:
        # modify here
        personal_computer:
            ansible_host: xxx.xxx.xxx.xxx # your server IP address
            ansible_user: "{{ variable_ssh_user | default(ansible_user_name) }}" # ansible remote user
            ansible_python_interpreter: /usr/bin/python3 # forcing python 3
```

Next, you must create your vault file in `inventories/group_vars/production/vault/common.yml`, containing password and the SHA salt for unique encrypting for the *main_user* user. 

Use the command below to start an ansible container using docker in interactive mode:

```
make exec
```

Now, use the following command to create a new vault file:

```
ansible-vault create inventories/group_vars/production/vault/common.yml
```

Currently used `vault.yml` template file:

```yml
---
# main_user
vault_main_user_pw: 'yourPassword' # change here
vault_main_user_pw_salt: yourPasswordSalt # change this value too
```

Then, you will also need to add the following execute the `inventories/group_vars/production/vault/common.yml`:

```yml
---
# main user password and salt
main_user_password_value: "{{ vault_main_user_password_value }}"
main_user_password_salt: "{{ vault_main_user_password_salt }}"
```

Doing this way you can fork this code and push everything to a remote repository without worrying about sensitive variables.

Now, execute the `bootstrap.yml` playbook against your server to install the basic Ansible requisites (Python 3.x) and to create the Ansible user responsible for managing your host, by providing a password SSH access with the `--ask-pass`, `--ask-vault-pass` and `--ask-become-pass` (optionak: if your user has a sudo password) flag:

```
ansible-playbook bootstrap.yml -vv -i inventories/production.yml --extra-vars "variable_ssh_user=your_user" --ask-pass --ask-vault-pass --ask-become-pass
```

**And finally, now you can run our main playbook against your host using the ansible user created from the bootstrap.yml playbook earlier:**

```
ansible-playbook playbook.yml -vv -i inventories/production.yml --ask-vault-pass
```

## TODO

### Vagrant:

- Test changes on VagrantFile;
- Create VagrantFile for kvm/qemu;
- Make kvm/qemu vagrantFile as default with Makefile;

### Local roles:

#### common:

- Add temp task to set timezone and locale since the current community task is not working;

#### dconf:

- Add better organization for keys and values to dconf var;

### Playbooks:

- Update instructions within this README;
- Convert boostrap playbook to role?
    - include_role or include_playbook with [ 'never', 'bootstrap' ] to be called only with command below:
    ```
        ansible-playbook playbook.yml -vv -i inventories/staging.yml --tags "boostrap"
    ```
- Add task to copy personal SSH key to *main_user*;
    - update ansible.dockerfile as well if needed;
- Add retries for some tasks;
    - mostly for the apt module;
- Fix "No such entitity: /home/pirasbro/.profile when logging with main_user;
    - Task below is the culprit:
        - Set audio default output source:
            [WARNING]: Module remote_tmp /home/pirasbro/.ansible/tmp did not exist and was created with a mode of 0700, this may cause issues when running as another user. To avoid this, create the remote_tmp dir with the correct permissions manually
- Add better way to create ansible vault files since the creation on this guide use the ansible container directly and everything is lost when the container is removed;
- Install gnome extensions for main_user;
    - Use dconf role to configure those extensions;
- Add customized boot grub theme;
    - Copy theme as well;
- Add "ansible managed" when using lineinfile and template files?
- Install [libratbag](https://github.com/libratbag/libratbag) - a DBus daemon to configure input devices, mainly gaming mice;
- add microk8s alias to .profile? (maybe not - bash completion doesn't work on aliases)
    - alias microk8s.kubectl=kubectl
- replace Virtual Box with QEMU, KVM, libvirt and virt-mananger stack;
    - also install vagrant-libvirt plugin OR USE NEW DOCKER IMAGE https://github.com/vagrant-libvirt/vagrant-libvirt#using-docker-based-installation;
    - qemu-kvm
    - libvirt-daemon-system
    - libvirt-clients
    - bridge-utils
    - virtinst
    - virt-manager
- Find a better CPU scheduler for desktop usage: BMQ, MuQSS, etc, and compile it;
- Find fix to xorg with Nvidia Drivers and multiple monitors with different refresh rates;