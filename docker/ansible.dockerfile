# This image is based on ansible-community/toolset:
# https://github.com/ansible-community/toolset/blob/fc002246f73b64948f263a559d68f0af75386e9f/Dockerfile

# Build requirements.txt stage
FROM python:3.9.1-slim-buster as builder

ENV PATH="/opt/ansible/bin:$PATH"
ENV PIP_INSTALL_ARGS="--pre"

# Copy requirements.in to container
COPY docker/requirements.in /tmp/requirements.in
RUN \
# Install pip and pip-tools and build requirements.txt file
    python3 -m pip install --no-cache-dir --upgrade 'pip>=21.0.1' \
    && python3 -m pip install --no-cache-dir 'pip-tools>=5.5.0' \
    && pip-compile \
        --allow-unsafe \
        --output-file=/tmp/requirements.txt \
            /tmp/requirements.in \
# Install requirements
    && python -m venv /opt/ansible \
    && python3 -m pip install --no-cache-dir --upgrade \
        ${PIP_INSTALL_ARGS} --requirement /tmp/requirements.txt


# Final stage
FROM python:3.9.1-slim-buster as final

ARG PACKAGES
ENV PACKAGES="\
curl \
nano \
vim \
iputils-ping \
"
ENV SHELL /bin/bash
ENV PYTHONDONTWRITEBYTECODE=1
ENV ANSIBLE_FORCE_COLOR=1
ENV PATH="/opt/ansible/bin:$PATH"

# Copy fake passwordless SSH Key to ansible_host container
COPY docker/fake_id_ed25519_ansible-host_key* /tmp/
# Copy packages from builder stage
COPY --from=builder /opt/ansible /opt/ansible

RUN set -uxo; \
# Enable prompt color and some aliases in the skeleton .bashrc
#   before creating ansible user
    sed -e 's/^#force_color_prompt=yes/force_color_prompt=yes/' \
        -e 's/^#export GCC_COLORS=/export GCC_COLORS=/' \
        -e 's/^#alias ll=/alias ll=/' \
        -e 's/^#alias la=/alias la=/' \
        -e 's/^#alias l=/alias l=/' \
            -i /etc/skel/.bashrc \
# Create ansible user/group
    && addgroup \
        --gid 2727 \
            ansible \
    && adduser \
        --disabled-login \
        --ingroup ansible \
        --gecos "ansible user" \
        --shell /bin/bash \
        --uid 2727 \
            ansible \
# Install openssh server and sshpass
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        openssh-server \
        sshpass \
# Initial ansible user setup
    && mkdir -p /home/ansible/.ssh/ \
    && touch /home/ansible/.ssh/authorized_keys \
    && touch /home/ansible/.ssh/known_hosts \
    && mv /tmp/fake_id_ed25519_ansible-host_key* /home/ansible/.ssh/ \
    && chmod 600 -R /home/ansible/.ssh/ \
    && chmod 700 /home/ansible/.ssh \
    && chmod 600 /home/ansible/.ssh/authorized_keys \
        /home/ansible/.ssh/known_hosts \
    && chmod 644 /home/ansible/.ssh/*.pub \
    && mkdir /home/ansible/src \
    && chown ansible:ansible -R \
        /home/ansible/.ssh \
        /home/ansible/src \
# Install PACKAGES and pip
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        ${PACKAGES} \
# Configure argcomplete (ansible autocomplete)
    && mkdir -p /home/ansible/.bash_completion.d \
        && chmod 700 /home/ansible/.bash_completion.d/ \
        && chown ansible:ansible -R /home/ansible/.bash_completion.d/ \
    && activate-global-python-argcomplete --dest /home/ansible/.bash_completion.d/ \
# Cleanup
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
        /var/lib/apt/lists/* \
        /tmp/* \
        /var/tmp/*

# Step down from root
USER ansible
# Change default workspace
WORKDIR /home/ansible/src/
# Copy requirements file to container
COPY requirements.yml .
# Install required collections and roles
RUN ansible-galaxy install --role-file requirements.yml
# Copy src files to container
COPY . .
# Expose SSH port
EXPOSE 22/tcp

# Start SSH service
CMD ["/usr/sbin/sshd", "-D"]


# Metadata
# OCI image specs
LABEL org.opencontainers.image.authors="Mateus Villar" \
        org.opencontainers.image.title="Ansible" \
        org.opencontainers.image.description="This image contains the Ansible tool and a openssh-server." \
        org.opencontainers.image.licenses="MIT"